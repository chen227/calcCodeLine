﻿#ifndef CALCCODE_H
#define CALCCODE_H

#include <QObject>
#include <QThread>

class QFile;
class QDir;

class CalcCode : public QThread
{
    Q_OBJECT
public:
    CalcCode(QObject *parent=0);
    ~CalcCode();

    void readFiles();

    void setPath(const QString &path);

    void calc(const QDir &dir);

    void calcLine(const QString path);

protected:
    void run();

signals:
    void getValue(const QVariantList &data);
    void getOneValue(const QVariantMap &data);

private:
    QString m_path;
    QVariantList m_list;
};

#endif // CALCCODE_H
