﻿#include "widget.h"
#include "ui_widget.h"
#include "CalcCode.h"
#include <QDir>
#include <QFileDialog>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    this->resize(800,600);

    this->setWindowTitle("计算代码行数");

    m_sizeNum = 0;
    m_fileNum = 0;
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    //open
    QString path = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                      QDir::currentPath(),
                                                      QFileDialog::ShowDirsOnly
                                                      | QFileDialog::DontResolveSymlinks);

    m_calcQThread  = new CalcCode(this);
    connect(m_calcQThread,SIGNAL(getValue(QVariantList)),this,SLOT(onGetValue(QVariantList)));
    connect(m_calcQThread,SIGNAL(getOneValue(QVariantMap)),this,SLOT(onGetOneValue(QVariantMap)));
    connect(m_calcQThread,SIGNAL(finished()),m_calcQThread,SLOT(deleteLater()));
    m_calcQThread->setPath(path);
    m_calcQThread->start();

    ui->listWidget->clear();
    m_sizeNum = 0;
    m_fileNum = 0;
}

void Widget::onGetValue(const QVariantList &data)
{
    ui->listWidget->addItem("总文件数：" + QString::number(m_fileNum) + "  总行数："+QString::number(m_sizeNum) + " 行");
}
//一项项获取item
void Widget::onGetOneValue(const QVariantMap &data)
{

//    if(data.value("type") == "h" || data.value("type") == "cpp" || data.value("type") == "qml"){

        QString filePath = data.value("filePath").toString();
        int lineNum = data.value("lineNum").toInt();

        m_sizeNum += lineNum;

        m_fileNum++;

        ui->listWidget->addItem((filePath + ": " + QString::number(lineNum) + " 行"));
//    }

}
