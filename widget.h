﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class CalcCode;

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_clicked();

    void onGetValue(const QVariantList &data);
    void onGetOneValue(const QVariantMap &data);


private:
    Ui::Widget *ui;
    CalcCode *m_calcQThread;
    int m_sizeNum;      //总行数
    int m_fileNum;      //总文件数
};

#endif // WIDGET_H
