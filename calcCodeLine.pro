#-------------------------------------------------
#
# Project created by QtCreator 2017-02-14T14:44:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = calcCodeLine
TEMPLATE = app
DESTDIR = $$PWD/bin

SOURCES += main.cpp\
        widget.cpp \
    CalcCode.cpp

HEADERS  += widget.h \
    CalcCode.h

FORMS    += widget.ui
